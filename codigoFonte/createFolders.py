# ~by: Hartur_comH
import os

root = input("Entre com o caminho que deseja criar as pastas (ex: 'C:/Propostos'): ")
tipo = input("Digite '0' para exercicios propostos e '1' para exercicios resolvidos: ")

if tipo == '0':
    path = root + '/ex_prop_'
elif tipo == '1':
    path = root + '/ex_res_'
else:
    print("Oporra é 0 ou 1")
    exit()

for i in range(1, 26):
    os.mkdir(path + str(i))