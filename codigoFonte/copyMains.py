# ~by: Hartur_comH
import shutil


root = input("Entre com o caminho onde estão os projetos \n(ex: C:/Users/SeuUsuario'/Documentos/NetBeansProjects): ")
destino = input("Entre com o caminho em que se encrontram as pastas(ex_res/prop_x): ")
tipo = input("Digite '0' para exercicios propostos e '1' para exercicios resolvidos: ")

if tipo == '0':
    root = root + '/EP'
    destino = destino + '/ex_prop_'
elif tipo == '1':
    root = root + '/ER'
    destino = destino + '/ex_res_'
else:
    print("Oporra é 0 ou 1")
    exit()

for i in range(1, 26):
    if i < 10:
        shutil.copy2(root + '0' + str(i) + '/main.c', destino + str(i) + '/main.c')
    else:
        shutil.copy2(root + str(i) + '/main.c', destino + str(i) + '/main.c')