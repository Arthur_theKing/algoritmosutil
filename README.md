# algoritmosUtil

Cria pastas da lista e copia os arquivos "main.c".

Criar pastas
* 1º passo:
*     Criar pastas para os exercicios resolvidos e propostos;
* 2º passo:
*     -Executar o arquivo "createFolders.exe";
*     -Indicar caminho da pasta destino;
*     -Escolher exercicios resolvidos ou propostos.

Copiar main.c
* 1º passo:
*   -Para que o algoritmo funcione é preciso que os projetos sajam criados com um
    padrão de nomeclatura, "ER0X" para os exercicios resolvidos e "EP0X" para
    exercicios propostos;
* 2º passo:
*     -Executar o arquivo "copyMains.exe";
*     -Entrar com o caminho da pasta raiz dos projetos;
*     -Entrar com o caminho da pasta destino (Onde foi criado as pastas no primeiro executavel);
*     -Escolher exercicios resolvidos ou propostos. 
